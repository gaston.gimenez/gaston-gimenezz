##############################################Este script se basa en Bind9.#################################################
############################################################################################################################
#						     SOLO USO ROOT							   #
# Con el podes crear distintas zonas con registros tipo A, CNAME, etc. Su principal funcionalidad es esa, despues	   #
# cuenta con chequeo de instalacion de Bind, chequeo de si esta prendido y demas.                                          #
# Todo el programa cuenta con validaciones de todo tipo, de ingresos de IPs, de ingreso de zonas creadas si las hubiese,   #
# de ingresos de numeros, etc.												   #
#															   #
# Tambien cuenta con creacion de ficheros de configuracion para la funcionalidad del bind.				   #
#                                                                                                                          #
# El codigo cuenta con argumentos, funciones, validaciones if, bucles while, variables, concatenaciones y arrays.	   #
#							AVISO:								   #
# Uso: Para configurar correctamente bind debe primero configurar con la opcion [file] y luego ya puede crear sus zonas.   #
############################################################################################################################
