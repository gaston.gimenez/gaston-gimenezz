#!/bin/bash

USO="MANUAL:
        USO: $0 [opciones] [parametros]

opciones:       file - Configura el archivo options de bind.
                zona - Crea zonas de bind.
                registro - Creas distintos tipos de registros.
		backup - Realiza backup del bind.

Parametros:     Usando cada opcion veras sus distintos argumentos."

echo "$USO"

