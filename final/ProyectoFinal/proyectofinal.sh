#!/bin/bash

# Para un mejor orden las variables estan a medida que las necesite, excepto las generales
RE='^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$'

# ------------------------------- Verificaciones iniciales -------------------------------

if [ $UID -ne 0 ]; then
        echo "Ejecute este programa como 'root'"
        exit 1
fi

if ! which named > /dev/null 2>&1 ; then
	echo "Instalando bind.." && \
	apt-get update > /dev/null 2>&1 && \
	apt-get -y install bind9 bind9-doc bind9utils && \
	echo "Instalacion finalizada!"
fi

if ! systemctl status bind9 > /dev/null 2>&1 ; then
	systemctl start bind9 > /dev/null 2>&1 && systemctl enable bind9 > /dev/null 2>&1
fi

if ! grep "\-4" /etc/default/bind9 > /dev/null 2>&1 ; then
        sed -i 's/OPTIONS\=\"\-u bind\"/OPTIONS\=\"\-u bind \-4\"/g' /etc/default/bind9 && \
	systemctl restart bind9
fi

# ------------------------------- Funciones -------------------------------

iph_val(){
	if [[ $iph =~ $RE ]]; then
   		VIFS=$IFS
    		IFS='.'
       		IP=($iph)
    		IFS=$VIFS
   		if [[ ${IP[0]} -le 255 && ${IP[1]} -le 255 && ${IP[2]} -le 255 && ${IP[3]} -le 255 && ${IP[4]} -le 32 ]]; then
			echo "Ip valida!"
			return 0
		else
			echo "Escriba una Ip valida por favor."
			return 1
		fi
		return 0
	else
		echo "Escriba una Ip valida por favor."
		return 1
	fi
}
ipz_val(){
        if [[ $ipz =~ $RE ]]; then
                VIFS=$IFS
                IFS='.'
                IP=($ipz)
                IFS=$VIFS
                if [[ ${IP[0]} -le 255 && ${IP[1]} -le 255 && ${IP[2]} -le 255 && ${IP[3]} -le 255 && ${IP[4]} -le 32 ]]; then
                        echo "Ip valida!"
                        return 0
                else
                        echo "Escriba una Ip valida por favor."
                        return 1
                fi
                return 0
        else
                echo "Escriba una Ip valida por favor."
                return 1
        fi
}
rip_val(){
        if [[ $RIP =~ $RE ]]; then
                VIFS=$IFS
                IFS='.'
                IP=($RIP)
                IFS=$VIFS
                if [[ ${IP[0]} -le 255 && ${IP[1]} -le 255 && ${IP[2]} -le 255 && ${IP[3]} -le 255 && ${IP[4]} -le 32 ]]; then
                        echo "Ip valida!"
                        return 0
                else
                        echo "Escriba una Ip valida por favor."
                        return 1
                fi
                return 0
        else
                echo "Escriba una Ip valida por favor."
                return 1
        fi
}

# ------------------------------- Case -------------------------------

case $1 in
man)
	bash man.sh
;;
file)
	if [[ $2 == options ]]; then
		read -p "Ingrese la ip de su maquina: " iph
			while ! iph_val ; do
				read -p "Ingrese la ip de su maquina: " iph
			done
conf="acl \"trusted\" {
        $iph;
};
options {
        directory "/var/cache/bind";
        recursion yes;
        allow-recursion { trusted; };
        listen-on { $iph; };
        allow-transfer { none; };

        forwarders {
                8.8.8.8;
                8.8.4.4;
        };

        dnssec-validation auto;

        listen-on-v6 { any; };
};"
		echo "Si quiere ingresar mas ips a la acl debe agregarlas en /etc/bind/named.conf.options, abajo de la suya"
	echo "$conf" > /etc/bind/named.conf.options
	else
		echo "Opcion invalida: Pruebe [file] [options]"
	fi
;;
zona)
	read -p "Ingrese un nombre para la zona: " nickz
	read -p "Ingrese la ip para la zona: " ipz
		while ! ipz_val ; do
			read -p "Ingrese la ip para la zona: " ipz
		done
	VIFS=$IFS && IFS='.' && aripz=($ipz) && IFS=$VIFS
zn="zone \"$nickz\" {
    type master;
    file "/etc/bind/zones/db.$nickz";
    allow-update { none; };
};"
rzn="zone \"${aripz[1]}.${aripz[0]}.in-addr.arpa\" {
    type master;
    file "/etc/bind/zones/db.${aripz[1]}.${aripz[0]}";
    allow-update { none; };
};" &&
	echo "$zn" >> '/etc/bind/named.conf.local' && \
	echo "$rzn" >> '/etc/bind/named.conf.local'
	echo "Ahora crearemos el file de la zona.."
	mkdir -p /etc/bind/zones
filz="\$TTL    604800
@       IN      SOA     $nickz. root.$nickz. (
			2		 ; Serial
			604800		 ; Refresh
			86400		 ; Retry
			2419200          ; Expire
			604800 )	 ; Negative Cache TTL
;"
filzr="\$TTL    604800
@       IN      SOA     $nickz. root.$nickz. (
			1		 ; Serial
			604800		 ; Refresh
			86400		 ; Retry
			2419200		 ; Expire
			604800 )	 ; Negative Cache TTL
;"
	echo "$filz" > "/etc/bind/zones/db.$nickz"
	echo "$filzr" > "/etc/bind/zones/db.${aripz[1]}.${aripz[0]}"
	echo "Creado con exito!"
;;
registro)
	if [[ $2 == A ]]; then
		read -p "Ingrese la zona para agregar registros: " rzon
		if grep $rzon '/etc/bind/named.conf.local' > /dev/null 2>&1 ; then
			read -p "Ingrese el dominio que quiere: " RA
			read -p "Ingrese la ip para el registro: " RIP
				while ! rip_val ; do
					read -p "Ingrese la ip para el registro: " RIP
				done
			echo "$RA	IN	A	$RIP" >> /etc/bind/zones/db.$rzon && \
			ras=`grep "Serial" /etc/bind/zones/db.$rzon | cut -f 1 -d ' '`
			read -p "El numero serial es $ras, ingrese el numero siguiente: " sernum
				while ! [[ $sernum  =~ ^[0-9]+$ ]] ; do
					read -p "El numero serial es $ras, ingrese el numero siguiente: " sernum
				done && sed -i "s/$ras \; Serial/			$sernum		 \; Serial/g" /etc/bind/zones/db.$rzon
		else
			echo "Ingrese una zona valida."
		fi
	elif [[ $2 == CNAME ]]; then
		read -p "Ingrese la zona para agregar registros: " rzon
                if grep $rzon '/etc/bind/named.conf.local' > /dev/null 2>&1 ; then
			read -p "Ingrese el sinonimo a la zona: " rcname
			echo "$rcname	IN	CNAME	$rzon" >> /etc/bind/zones/db.$rzon && \
			ras=`grep "Serial" /etc/bind/zones/db.$rzon | cut -f 1 -d ' '`
                        read -p "El numero serial es $ras, ingrese el numero siguiente: " sernum
                                while ! [[ $sernum  =~ ^[0-9]+$ ]] ; do
                                        read -p "El numero serial es $ras, ingrese el numero siguiente: " sernum
                                done && sed -i "s/$ras \; Serial/			$sernum		 \; Serial/g" /etc/bind/zones/db.$rzon
		else
			echo "Ingrese una zona valida."
		fi
	elif [[ $2 == NS ]]; then
		read -p "Ingrese la zona para agregar registros: " rzon
                if grep $rzon '/etc/bind/named.conf.local' > /dev/null 2>&1 ; then
			read -p "Ingrese el NS para la zona: " rns
				echo "$rns	IN	NS	$rzon" >> /etc/bind/zones/db.$rzon && \
			ras=`grep "Serial" /etc/bind/zones/db.$rzon | cut -f 1 -d ' '`
                        read -p "El numero serial es $ras, ingrese el numero siguiente: " sernum
                                while ! [[ $sernum  =~ ^[0-9]+$ ]] ; do
                                        read -p "El numero serial es $ras, ingrese el numero siguiente: " sernum
                                done && sed -i "s/$ras \; Serial/			$sernum		 \; Serial/g" /etc/bind/zones/db.$rzon
		else
			echo "Ingrese una zona valida."
		fi
	else
		echo "Opcion invalida: Pruebe [registro] [A/NS/CNAME]"
	fi
;;
backup)
	echo "Realizando backup.."
	mkdir -p /var/backups && \
	cp -R /etc/bind/ /var/backups && \
	mv /var/backups/bind/ "/var/backups/bind_$(date)_bk" && echo "Backup realizado con exito"
;;
*)
	echo "Para su uso consulte $0 [man]"
;;
esac
