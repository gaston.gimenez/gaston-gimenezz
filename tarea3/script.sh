#!/bin/bash

# VERIFICACION DE QUE SEA ROOT
echo "AVISO: Este programa debe ejecutarse como root."
echo " "
echo "Analizando si es root.."

sleep 1s

if [ $UID -ne 0 ]; then
	echo "Ejecute este programa como 'root'"
	exit 1
fi

echo "Parte 1.."
sleep 1s
# --------------------------------------1 PARTE-----------------------------------------------------

USO="Permisos:
Escritura: '2'
Lectura: '4'
Ejecucion: '1'

El primer numero para user, segundo para grupo y tercero para demas usuarios.
Ej: '745' ([7]wrx 'usuarios':[4]r 'grupo': [5]rx 'other users')
Con '000' sacas todos los permisos."

RE='^[0-9]+$'

read -e -p "Escriba la ruta completa del archivo o directorio que quiere cambiarle los permisos: " ARCH

if [ ! -z $ARCH ]; then
	echo " "
	echo "Para introducir los numeros lea la siguiente guia de permisos:"
	echo "$USO"
	echo " "
else
	read -e -p "Escriba la ruta completa del archivo o directorio como se le pide: " ARCH
	echo " "
        echo "Para introducir los numeros lea la siguiente guia de permisos:"
        echo "$USO"
        echo " "
fi
sleep 1s

read -p "Ahora escriba el conjunto de permisos(en numeros): " PER

# VERIFICACION DE QUE INTRODUZCAN NUMEROS VALIDOS
if [[ $PER =~ $RE && $PER -ge 0 && $PER -le 777 ]]; then

        chmod $PER $ARCH > /dev/null 2>&1
else
        echo "Introduzca numeros como se pide por favor."
sleep 1s
        read -p "Escriba el conjunto de permisos nuevamente(en numeros): " PER
	chmod $PER $ARCH > /dev/null 2>&1
fi

echo "Asi quedo su archivo ''$(ls -l $ARCH)''"

sleep 1s
echo "Parte 2.."
sleep 1s
# --------------------------------------2 PARTE-----------------------------------------------------

CK="/etc/group"

read -p "Escriba el nombre del grupo que desea crear(blank no crea ninguno): " ADGP

if [ ! -z $ADGP ]; then
        groupadd $ADGP > /dev/null 2>&1 && \
        echo "Grupo creado con exito: " && cat $CK | grep $ADGP
fi

sleep 1s

read -p "Escriba el nombre del grupo que desea eliminar(blank no elimina ninguno): " DDGP

if [ ! -z $DDGP ]; then
        echo "Eliminando $DDGP.."
        if grep $DDGP $CK > /dev/null 2>&1 ; then
                groupdel $DDGP
        else
                echo "Introduzca un grupo existente por favor."
                read -p "Escriba el nombre del grupo que desea eliminar: " DDGP
                groupdel $DDGP > /dev/null 2>&1
        fi
fi

sleep 1s
echo "Parte 3.."
sleep 1s
# --------------------------------------3 PARTE-----------------------------------------------------

CK="/etc/group"
UC="/etc/passwd"

read -e -p "Escriba la ruta completa del archivo o directorio que quiere cambiarle el dueño o grupo: " GR

if [ -z $GR ]; then
	read -e -p "Escriba la ruta completa del archivo o directorio como se le pide por favor: " GR

fi

read -p "Si desea cambiar el usuario del archivo anterior escribalo(blank no realiza ningun cambio): " USER


if [ ! -z $USER ]; then

        if grep $USER $UC > /dev/null 2>&1 ; then
                echo "Usuario: $USER"
        else
                echo "Introduzca un usuario existente por favor."
                read -p "Escriba el nombre del usuario nuevamente: " USER
        fi
fi

read -p "Si desea cambiar el grupo del archivo anterior escribalo(blank no realiza ningun cambio): " GROUP

if [ ! -z $GROUP ]; then

        if grep $GROUP $CK > /dev/null 2>&1 ; then
                echo "Grupo: $GROUP"
        else
                echo "Introduzca un grupo existente por favor."
                read -p "Escriba el nombre del grupo nuevamente: " GROUP
        fi
fi
sudo chown $USER:$GROUP $GR

echo "Asi quedo su archivo ''$(ls -l $GR)''"

sleep 1s
echo "Parte 4.."
sleep 1s
# --------------------------------------4 PARTE-----------------------------------------------------

echo "Estos son los grupos del sistema:"

	cat -n /etc/group | cut -d ":" -f1

# --------------------------------------5 PARTE-----------------------------------------------------
sleep 1s
echo "Parte 5.."

read -e -p "Especifique la ruta donde quiere buscar el archivo: " DIR

echo " "
echo "$USO"
echo " "
sleep 1s

read -p "Especifique el conjunto de permisos para la busqueda(en numeros): " PERMI

if [[ $PERMI =~ $RE && $PERMI -ge 0 && $PERMI -le 777 ]]; then

	find $DIR -perm $PERMI
else
        echo "Introduzca numeros como se pide por favor."
sleep 1s
        read -p "Especifique el conjunto de permisos para la busqueda: " PERMI
	find $DIR -perm $PERMI
fi

sleep 1s
echo "---------------------El exit status final es: $? ---------------------"
