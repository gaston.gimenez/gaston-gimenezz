#!/bin/bash

USO="MANUAL:
	USO: $0 [opciones] [parametros]

opciones:       habilitar - Prende el fw.
                deshabilitar - Apaga el fw.
                status - Status del fw.
                listar - Lista reglas del fw.
		reiniciar - Resetea el fw a default.
		input - Controla cadena input.
		output - Controla cadena output.
 		forward - Controla cadena forward.
 		puerto - Crea reglas de puertos.
 		ips - Crea reglas de puertos con origen y destino especifico.
		insertar - Agregas una regla en la posicion de lista que quieras.
		backup - Realiza backups de las reglas actuales.
 		eliminar - Elimina alguna regla mediante su numero de lista.

parametros: 	Usando cada opcion veras sus distintos argumentos."

echo "$USO"
