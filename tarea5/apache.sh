#!/bin/bash

if [ $UID -ne 0 ]; then
        echo "Ejecute este programa como 'root'"
        exit 1
fi

if ! which apache2 > /dev/null 2>&1 ; then
       echo "Instalando apache2.."
       apt-get update > /dev/null 2>&1 && \
       apt-get -y install apache2 && echo "Instalado correctamente"
fi

if ! systemctl status apache2; then
	systemctl start apache2
fi
# -------------------------- Variables -----------------------------

EX='^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$' # Expresion regular para que tome formato ip
DIR='/etc/network/interfaces' # Directorio de interfaces
DAT=`grep 'address' $DIR | cut -f 2 -d' '` # Variable para cambiar IP
SITE="/var/www/$3"
SITIO="/etc/apache2/sites-available/$3.conf"
DEF="	<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        # Directorio de los ficheros del sitio
        DocumentRoot $SITE
        # Nombre del vhost
        ServerName $3
        # Logs
       	ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog /var/log/apache2/$3.log combined
	</VirtualHost>"
PH="<?php
	phpinfo();
	phpinfo(INFO_MODULES);
?>"
STACK="/var/www/$3/info.php"

LOG="/var/log/apache2/$3.log"
ETC='/etc/apache2/sites-available'

U="Para su uso consulte
        $0 [man]" # Variable para uso

# Hay variables declaradas en algunos case

# -------------------------- Funciones -----------------------------
ip_valida(){
        if [[ $ipnu =~ $EX ]];then
                VIFS=$IFS
                IFS='.'
                IP=($ipnu)
                IFS=$VIFS
                if [[ ${IP[0]} -le 255 && ${IP[1]} -le 255 && ${IP[2]} -le 255 && ${IP[3]} -le 255 ]]; then
                        sed -i "s/$DAT/$ipnu/g" $DIR
			return 0
                else
                        echo "ip incorrecta."
			return 1
                fi
                return 0
        else
                echo "ip incorrecta."
                return 1
        fi
}
ip_val(){
        if [[ $ipem =~ $EX ]];then
                VIFS=$IFS
                IFS='.'
                IP=($ipem)
                IFS=$VIFS
                if [[ ${IP[0]} -le 255 && ${IP[1]} -le 255 && ${IP[2]} -le 255 && ${IP[3]} -le 255 ]]; then
                        echo "Ip valida."
                        return 0
                else
                        echo "ip incorrecta."
                        return 1
                fi
                return 0
        else
                echo "ip incorrecta."
                return 1
        fi
}
ipval_mig(){
        if [[ $IPVAL =~ $EX ]];then
                VIFS=$IFS
                IFS='.'
                IP=($IPVAL)
                IFS=$VIFS
                if [[ ${IP[0]} -le 255 && ${IP[1]} -le 255 && ${IP[2]} -le 255 && ${IP[3]} -le 255 ]]; then
                        return 0
                else
                        echo "Ingrese una ip valida."
                        return 1
                fi
                return 0
        else
                echo "Ingrese una ip valida."
                return 1
        fi
}
ipval_bck(){
        if [[ $bkip =~ $EX ]];then
                VIFS=$IFS
                IFS='.'
                IP=($bkip)
                IFS=$VIFS
                if [[ ${IP[0]} -le 255 && ${IP[1]} -le 255 && ${IP[2]} -le 255 && ${IP[3]} -le 255 ]]; then
                        return 0
                else
                        echo "Ingrese una ip valida."
                        return 1
                fi
                return 0
        else
                echo "Ingrese una ip valida."
                return 1
        fi
}

# -------------------------- Case -----------------------------

case $1 in

man)
	bash man.sh
;;
ip)
        if grep 'address' $DIR > /dev/null 2>&1 ; then
                read -p "Ingrese la nueva ip: " ipnu
                while ! ip_valida ; do
                        read -p "Ingrese una ip valida: " ipnu
                done && echo "Para realizar los cambios debe reiniciar la maquina." && \
        read -p "Desea reiniciarla ahora?[Y/n]" reinicio
                case $reinicio in
                yes|y|Y|si|YES|SI)
                                reboot
                ;;
                *)
                echo "Reinicio cancelado."
                exit 0
		;;
                esac
        else
                echo "Para poder cambiar la ip tiene que tener configurado una ip estatica primero."
        fi
;;
hostname)
        if [[ ! -z $2 ]]; then
                hostnamectl set-hostname $2 && echo "Para realizar los cambios debe reiniciar la maquina." && \
                read -p "Desea reiniciarla ahora?[Y/n]" reini
                case $reini in
                yes|y|Y|si|YES|SI)
                                reboot
                ;;
                *)
                echo "Reinicio cancelado."
                exit 0
		;;
                esac
        else
                echo "Opcion invalida: Pruebe [hostname] ['Nuevo nombre de hostname']"
        fi
;;
instalar)

        if [[ $2 == php ]]; then
                echo "Instalando.." && \
                apt-get update > /dev/null 2>&1 && \
                apt-get -y install php libapache2-mod-php php-mysql && echo "Instalado correctamente" && \
		systemctl restart apache2
        elif [[ $2 == mysql ]]; then
                echo "Instalando.." && \
                apt-get update > /dev/null 2>&1 && \
                apt-get -y install default-mysql-server && echo "Instalado correctamente" && \
		systemctl restart apache2
        elif [[ $2 == pack ]]; then
                echo "Instalando.." && \
                apt-get update > /dev/null 2>&1 && \
                apt-get -y install default-mysql-server php libapache2-mod-php php-mysql && \
                echo "Instalado correctamente" && systemctl restart apache2
        else
                echo "Opcion invalida: Pruebe [instalar] [php/mysql/pack]"
        fi
;;
desinstalar)
        if [[ $2 == php ]]; then
                echo "Desinstalando.." && \
                apt-get -y remove --purge php7* php* && \
                echo "Desinstalado correctamente"
        elif [[ $2 == mysql ]]; then
                echo "Desinstalando.." && \
                apt-get -y remove --purge mysql* && \
                echo "Desinstalado correctamente"
	elif [[ $2 == apache2 ]]; then
		echo "Desinstalando.." && \
		apt-get -y remove --purge apache2* apache2-* && \
		echo "Desinstalado correctamente."
        elif [[ $2 == pack ]]; then
                echo "Desinstalando.." && \
                apt-get -y remove --purge mysql* php7* php* apache2* apache2-* && \
                echo "Desinstalado correctamente"
        else
                echo "Opcion invalida: Pruebe [desinstalar] [php/mysql/apache2/pack]"
        fi
;;
sitio)

        if [[ $2 == crear && ! -z $3 ]]; then
                echo "Creando files necesarios.." && echo " " && \
                touch $SITIO && echo "Archivo: '$SITIO' Creado!" && echo "$DEF" > $SITIO && \
                mkdir $SITE && echo "Directorio: '$SITE' Creado!" && \
		echo "$PH" > $STACK && echo "Archivo: '$STACK' Creado!"&& \
                echo "Generando index.html para comprobar la pagina.." && \
                echo "<h1>Hola mundo</h1>" > $SITE/index.html && \
                echo "Activando el sitio.." && \
                a2ensite $3 > /dev/null 2>&1 && echo "Sitio activado!" && \
		addrip=`grep 'address' $DIR | cut -f 2 -d ' '` && echo "Su ip es $addrip" && \
                systemctl reload apache2 > /dev/null 2>&1
                read -p "Introduzca ip para emular un dominio: " ipem
                while ! ip_val ; do
                        read -p "Ingrese una ip valida: " ipem
                done && echo "$ipem $3" >> /etc/hosts
                echo "Pruebe un ping $3"
        elif [[ $2 == desactivar ]] && grep $3 $SITIO > /dev/null 2>&1 ; then
                a2dissite $3 > /dev/null 2>&1 && echo "Sitio desactivado!" && \
                read -p "Desea tambien borrar los archivos pertenecientes al sitio desactivado?[Y/n]" borro
                case $borro in
                yes|y|Y|si|YES|SI)
				host=`grep $3 /etc/hosts` && sed -i "/$host/d" /etc/hosts && \
                                rm -r -f $SITIO && rm -r -f $SITE && rm -r -f $LOG && \
                                echo "Archivos de $3 eliminados!"
                ;;
                *)
                echo "No se elimino ningun archivo."
                exit 0
		;;
                esac
        elif [[ $2 == activar ]] && grep $3 $SITIO > /dev/null 2>&1 ; then
                systemctl reload apache2 > /dev/null 2>&1
                a2ensite $3 > /dev/null 2>&1 && echo "Sitio activado!"
        else
                echo "Opcion invalida: Pruebe [sitio] [crear/activar/desactivar] [nombre del sitio]"
        fi
;;
wordpress)
        read -p "Para crear un sitio con wordpress necesita tener MYSQL y PHP descargado y cumplir una serie de pasos. Sigue?[Y/n]" rta
        case $rta in
        yes|Y|YES|y|si|SI)
                        if which mysql php > /dev/null ; then
                        echo "Creando base de datos 'wordpress'.."
                        mysql -e "CREATE DATABASE wordpress;" && echo "Base de datos creada." && \
                        read -p "Ingrese nombre de usuario para la base de datos: " us
                        while ! [[ $us  =~ ^[a-Z]+$ ]] ; do
                                read -p "Ingrese nombre de usuario correcto(SOLO NOMBRE:Ej. Juan): " us
                        done
                        read -p "Indique la contraseña de usuario: " ct
                        while ! [[ $ct  =~ ^[A-Z][a-z]+[0-9]$ ]] ; do
                                read -p "Debe tener una mayuscula inicial y un numero al final: " ct
                        done
                        mysql -e "GRANT ALL ON wordpress.* TO '$us'@'localhost' IDENTIFIED BY '$ct';" && \
                        echo "Usuario con privilegios creado!" && \
                        mysql -e "FLUSH PRIVILEGES;"
                        read -p "Ingrese el nombre de su sitio (Ej. facebook.com): " dom
                        while ! grep $dom "$ETC/$dom.conf" > /dev/null 2>&1 ; do
                                read -p "Ingrese el nombre de un sitio valido: " dom
                        done && WW="/var/www/$dom" && CFG="$WW/wordpress/wp-config-sample.php" && BCFG="$WW/wordpress/wp-config.php" && \
                        wget -P "$WW" https://wordpress.org/latest.tar.gz
                        tar -x -f $WW/latest.tar.gz -C $WW && cp $CFG $BCFG && \
			echo "Termino el proceso." && systemctl reload apache2.service
                        else
                                echo "MYSQL o PHP no instalado."
                        fi
			;;
			*)
			exit 0
			;;
			esac
;;
migrar)
        MIGSI="/etc/apache2/sites-available/$3.conf"
        MIG='/etc/apache2/sites-available'
        MIDIR="/var/www/$3"
        MIDI='/var/www'
        IPVAL="$2"
        if ipval_mig $IPVAL && grep $3 $MIGSI > /dev/null 2>&1 ; then
                scp $MIGSI root@$IPVAL:$MIG && scp -r $MIDIR root@$IPVAL:$MIDI && \
		echo "Sitio migrado." && \
		echo "Use ssh para activarlo."
        else
        echo "Opcion invalida: Pruebe [migrar] [ip destino] [nombre del sitio a migrar]"
        fi
;;
estadisticas)
        CONF="/etc/apache2/sites-available/$2.conf"
        LG="/var/log/apache2/$2.log"
        if grep $2 $CONF > /dev/null 2>&1 ; then
                vis=`wc -l $LG | cut -f 1 -d ' '` && echo "La pagina recibio: $vis visitas." && \
                wrd=`grep 'wordpress' -c $LG` && echo "La seccion 'wordpress' fue solicitada: $wrd veces." && \
                inf=`grep 'info.php' -c $LG` && echo "La seccion 'info.php' fue solicitada: $inf veces." && \
                echo "Las ips que ingresaron fueron:" && \
                while read -a LINE ; do
                        IPSTAT="${LINE[0]}"
                        echo "$IPSTAT"
                done < "$LG"
        else
        echo "Opcion invalida: Pruebe [estadisticas] [nombre del sitio]"
        fi
;;
backup)
        bkps="/etc/apache2"
        bkpv="/var/www"
        bkpl="/var/log/apache2"
        if which rsync > /dev/null 2>&1 ; then
                read -p "Desea realizar el backup de manera local o en otro server?[local/otro] " backup
                if [[ $backup == 'local' ]]; then
                        sed -i 's/\#PermitRootLogin prohibit-password/PermitRootLogin YES/g' /etc/ssh/sshd_config > /dev/null 2>&1
                        systemctl reload sshd
                        mkdir -p $HOME/apache/backups && \
                        rsync -ab $bkps $bkpv $bkpl root@127.0.0.1:$HOME/apache/backups && \
                        echo "Backup realizado y almacenado en: '$HOME/apache/backups'"
                elif [[ $backup == 'otro' ]]; then
                        read -p "Ingrese usuario del servidor destino: " usu
                        read -p "Ingrese ip del servidor destino: " bkip
                                while ! ipval_bck ; do
                                        read -p "Ingrese una ip valida: " bkip
                                done
                        read -p "Ingrese ruta del servidor destino: " ruta
                        rsync -ab $bkps $bkpv $bkpl $usu@$bkip:$ruta && \
                        echo "Backup realizado"
                else
                        echo "Porfavor coloque 'local' o 'otro'"
                fi
        else
                apt update > /dev/null 2>&1 && apt install -y rsync > /dev/null 2>&1 && \
                echo "Se instalo 'rsync'. Intente de nuevo!"
        fi
;;
*)
	echo "$U"

;;
esac
